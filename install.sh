#!/usr/bin/env bash
IFS=$'\n'
VERSION="0.0.1"
MOD_NAME="pizzatorio"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if test -f .sync-destination
then
	DEST="$( cat .sync-destination )/${MOD_NAME}_${VERSION}"
else
	echo """Please provide a destination folder in a file \`.sync-destination\`.
For example, put the following value in the \`.sync-destination\` (no trailing slash).

	/Users/me/Library/Application Support/factorio/mods
	"""
	exit 1
fi
SRC="${DIR}/src/main/lua/"

echo "Running synchronization from ${DIR}"
echo "Copying ${SRC} to ${DEST} forever... "
echo "Press [CTRL+C] to stop.."

while true
do
	TIMESTAMP=$(date +%T)
    echo "${TIMESTAMP} - Syncing files"
    rsync -a --itemize-changes --delete ${SRC} ${DEST} | grep '^>' | awk '{ print $2 }'
	sleep 5
done
