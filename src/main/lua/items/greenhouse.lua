local greenhouse = {
	type = "item",
	name = "greenhouse",
	icons = {
		{
			icon = "__base__/graphics/icons/lab.png",
			tint = { r = 0.34, g = 0.72, b = 0.28, a = 0.6 }
		}
	},
	icon_size = 32,
	stack_size = 10,
	place_result = "greenhouse"
}

data:extend{greenhouse}
