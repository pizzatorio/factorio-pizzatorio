local mushroom = {
	type = "item",
	name = "mushroom",
	icon = "__pizzatorio__/graphics/icons/mushroom.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{mushroom}
