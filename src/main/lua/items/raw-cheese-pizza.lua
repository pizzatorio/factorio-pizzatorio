local raw_cheese_pizza = {
	type = "item",
	name = "raw-cheese-pizza",
	icon = "__pizzatorio__/graphics/icons/raw-cheese-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{raw_cheese_pizza}
