local raw_fisheroni_pizza = {
	type = "item",
	name = "raw-fisheroni-pizza",
	icon = "__pizzatorio__/graphics/icons/raw-fisheroni-pizza.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{raw_fisheroni_pizza}
