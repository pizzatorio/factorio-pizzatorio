local snack_pack = {
	type = "item",
	name = "snack-pack",
	icon = "__base__/graphics/entity/wooden-chest/wooden-chest.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{ snack_pack }
