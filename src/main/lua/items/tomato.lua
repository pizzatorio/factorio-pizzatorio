local tomato = {
	type = "item",
	name = "tomato",
	icon = "__pizzatorio__/graphics/icons/tomato.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{tomato}
