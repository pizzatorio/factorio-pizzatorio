local yeast = {
	type = "item",
	name = "yeast",
	icon = "__pizzatorio__/graphics/icons/yeast.png",
	icon_size = 32,
	stack_size = 50
}

data:extend{yeast}
