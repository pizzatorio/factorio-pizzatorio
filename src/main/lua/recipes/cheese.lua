local cheese_recipe = {
	type = "recipe",
	name = "cheese",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "milk",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "cheese",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/cheese.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{cheese_recipe}
