local fish_recipe = {
	type = "recipe",
	name = "fish",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "water",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "raw-fish",
			amount = 1
		}
	},
	energy = 1,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{fish_recipe}
