local fisheroni_pizza_recipe = {
	type = "recipe",
	name = "fisheroni-pizza",
	enabled = true,
	hidden = false,
	category = "smelting",
	ingredients = {
		{
			type = "item",
			name = "raw-fisheroni-pizza",
			amount = 1
		}
	},
	results = {
		{
			type = "item",
			name = "fisheroni-pizza",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/fisheroni-pizza.png",
	icon_size = 32,
	energy = 10,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{fisheroni_pizza_recipe}
