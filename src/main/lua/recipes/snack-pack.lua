local snack_pack_recipe = {
	type = "recipe",
	name = "snack-pack",
	enabled = true,
	hidden = false,
	category = "crafting",
	ingredients = {
		{
			type = "item",
			name = "cheese-pizza",
			amount = 2
		}
	},
	results = {
		{type="item", name="snack-pack", amount=1}
	},
	icon = "__base__/graphics/entity/wooden-chest/wooden-chest.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other",
	order = "a"
}

data:extend{ snack_pack_recipe }
