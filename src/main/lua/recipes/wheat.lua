local wheat_recipe = {
	type = "recipe",
	name = "wheat",
	enabled = true,
	hidden = false,
	category = "crafting-with-fluid",
	ingredients = {
		{
			type = "fluid",
			name = "water",
			amount = 10
		}
	},
	results = {
		{
			type = "item",
			name = "wheat",
			amount = 1
		}
	},
	icon = "__pizzatorio__/graphics/icons/wheat.png",
	icon_size = 32,
	energy = 100,
	group = "intermediate-products",
	subgroup = "other"
}

data:extend{wheat_recipe}
